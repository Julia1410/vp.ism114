/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.mirea.iis.vp.bezgubova.labs.second;

/**
 *
 * @author q
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class Valuta extends JFrame {

    JPanel buttonPanel = new JPanel(new GridLayout(7, 5));
    JLabel label = new JLabel("Выберите операцию:");
    JButton btn0 = new JButton("Перевести российский рубль (RUB) в доллар США (USD)");
    JButton btn1 = new JButton("Перевести российский рубль (RUB) в евро (EUR)");
    JButton btn2 = new JButton("Перевести российский рубль (RUB) в британский фунт (GBP)");
    JButton btn3 = new JButton("Перевести доллар США (USD) в российский рубль (RUB)");
    JButton btn4 = new JButton("Перевести евро (EUR) в российский рубль (RUB)");
    JButton btn5 = new JButton("Перевести британский фунт (GBP) в российский рубль (RUB)");
    JButton btn6 = new JButton("Перевести доллар США (USD) в евро (EUR)");

    Valuta() throws IOException {

        FileInputStream fis = new FileInputStream("C:/Users/q/Documents/vp.ism114/bezgubova.labs/src/main/java/ru/mirea/iis/vp/bezgubova/labs/second/read.xls");
        Workbook wb = new HSSFWorkbook(fis);
        final double usd = wb.getSheetAt(0).getRow(0).getCell(1).getNumericCellValue();
        final double eur = wb.getSheetAt(0).getRow(1).getCell(1).getNumericCellValue();
        final double gbp = wb.getSheetAt(0).getRow(2).getCell(1).getNumericCellValue();


        setBounds(300, 300, 500, 400);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(buttonPanel, BorderLayout.CENTER);
        add(label, BorderLayout.NORTH);
        buttonPanel.add(btn0);
        buttonPanel.add(btn1);
        buttonPanel.add(btn2);
        buttonPanel.add(btn3);
        buttonPanel.add(btn4);
        buttonPanel.add(btn5);
        buttonPanel.add(btn6);

        setVisible(true);



        btn0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / usd;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " USD");
            }
        });
        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / eur;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " EUR");
            }
        });
        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / gbp;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " GBP");
            }
        });
        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (USD):"));
                double a = value * usd;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " USD " + "= " + r + " RUB");
            }
        });
        btn4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (EUR):"));
                double a = value * eur;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " EUR " + "= " + r + " RUB");
            }
        });
        btn5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (GBP):"));
                double a = value * gbp;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " GBP " + "= " + r + " RUB");
            }
        });

        btn6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (USD):"));
                double a = value * (eur / usd);
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " USD " + "= " + r + " EUR");
            }
        });

    }

    public static void main(String[] args) throws IOException {
        new Valuta();

    }
}

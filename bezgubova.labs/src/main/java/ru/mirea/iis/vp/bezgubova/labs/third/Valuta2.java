/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.mirea.iis.vp.bezgubova.labs.third;

/**
 *
 * @author q
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class Valuta2 extends JFrame {

    JPanel buttonPanel = new JPanel(new GridLayout(6, 5));
    JLabel label = new JLabel("Выберите операцию:");
    JButton btn0 = new JButton("Перевести российский рубль (RUB) в доллар США (USD)");
    JButton btn1 = new JButton("Перевести российский рубль (RUB) в евро (EUR)");
    JButton btn2 = new JButton("Перевести российский рубль (RUB) в британский фунт (GBP)");
    JButton btn3 = new JButton("Перевести доллар США (USD) в российский рубль (RUB)");
    JButton btn4 = new JButton("Перевести евро (EUR) в российский рубль (RUB)");
    JButton btn5 = new JButton("Перевести британский фунт (GBP) в российский рубль (RUB)");
    JButton btn6 = new JButton("Перевести доллар США (USD) в евро (EUR)");
    JButton btn7 = new JButton("Перевести российский рубль (RUB) в Канадский доллар (CAD)");
    JButton btn8 = new JButton("Перевести российский рубль (RUB) в Бразильский реал (BRL)");
    JButton btn9 = new JButton("Перевести российский рубль (RUB) в Швейцарский франк (CHF)");
    JButton btn10 = new JButton("Перевести Швейцарский франк (CHF) в российский рубль (RUB)");
    JButton btnk = new JButton("Просмотр курса валют");

    Valuta2() throws IOException {

        FileInputStream fis = new FileInputStream("C:/Users/q/Documents/vp.ism114/bezgubova.labs/src/main/java/ru/mirea/iis/vp/bezgubova/labs/third/read.xls");
        Workbook wb = new HSSFWorkbook(fis);
        final double usd = wb.getSheetAt(0).getRow(0).getCell(1).getNumericCellValue();
        final double eur = wb.getSheetAt(0).getRow(1).getCell(1).getNumericCellValue();
        final double gbp = wb.getSheetAt(0).getRow(2).getCell(1).getNumericCellValue();
        final double cad = wb.getSheetAt(0).getRow(3).getCell(1).getNumericCellValue();
        final double brl = wb.getSheetAt(0).getRow(4).getCell(1).getNumericCellValue();
        final double chf = wb.getSheetAt(0).getRow(5).getCell(1).getNumericCellValue();


        setBounds(300, 300, 1000, 350);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(buttonPanel, BorderLayout.CENTER);
        add(label, BorderLayout.NORTH);

        buttonPanel.add(btn0);
        buttonPanel.add(btn3);
        buttonPanel.add(btn1);
        buttonPanel.add(btn4);
        buttonPanel.add(btn2);
        buttonPanel.add(btn5);
        buttonPanel.add(btn7);
        buttonPanel.add(btn6);
        buttonPanel.add(btn8);
        buttonPanel.add(btn10);
        buttonPanel.add(btn9);
        buttonPanel.add(btnk);
        setVisible(true);


        btn0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / usd;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " USD");
            }
        });
        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / eur;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " EUR");
            }
        });
        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / gbp;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " GBP");
            }
        });
        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (USD):"));
                double a = value * usd;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " USD " + "= " + r + " RUB");
            }
        });
        btn4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (EUR):"));
                double a = value * eur;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " EUR " + "= " + r + " RUB");
            }
        });
        btn5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (GBP):"));
                double a = value * gbp;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " GBP " + "= " + r + " RUB");
            }
        });

        btn6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (USD):"));
                double a = value * (usd / eur);
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " USD " + "= " + r + " EUR");
            }
        });

        btn7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / cad;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " CAD");
            }
        });

        btn8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / brl;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " BRL");
            }
        });
        btn9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (RUB):"));
                double a = value / chf;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " RUB " + "= " + r + " CHF");
            }
        });

        btn10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double value = Double.parseDouble(JOptionPane.showInputDialog("Введите сумму (CHF):"));
                double a = value * chf;
                NumberFormat formatter = NumberFormat.getNumberInstance();
                formatter.setMaximumFractionDigits(2);
                String r = formatter.format(a);
                JOptionPane.showMessageDialog(null, value + " CHF " + "= " + r + " RUB");
            }
        });

        btnk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null, "1 USD (Доллар США) = " + usd + " RUB\n"
                        + "1 EUR (Евро) = " + eur + " RUB\n"
                        + "1 GBP (Фунт стерлингов Соединенного королевства) = " + gbp + " RUB\n"
                        + "1 CAD (Канадский доллар) = " + cad + " RUB\n1 BRL (Бразильский реал) = " + brl + " RUB\n1 CHF (Швейцарский франк) = " + chf + " RUB");

            }
        });

    }

    public static void main(String[] args) throws IOException {
        new Valuta2();

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.mirea.iis.vp.bezgubova.labs.first;

/**
 *
 * @author q
 */
import java.io.FileInputStream;
import java.util.*;
import java.io.IOException;
import javax.swing.JFrame;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class App extends JFrame {

    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("C:/git_tmp/read.xls");
        Workbook wb = new HSSFWorkbook(fis);
        double usd = wb.getSheetAt(0).getRow(0).getCell(1).getNumericCellValue();
        double eur = wb.getSheetAt(0).getRow(1).getCell(1).getNumericCellValue();
        double gbp = wb.getSheetAt(0).getRow(2).getCell(1).getNumericCellValue();
        double cad = wb.getSheetAt(0).getRow(3).getCell(1).getNumericCellValue();
        double brl = wb.getSheetAt(0).getRow(4).getCell(1).getNumericCellValue();
        double chf = wb.getSheetAt(0).getRow(5).getCell(1).getNumericCellValue();

        Scanner input = new Scanner(System.in);
        double summa;
        double val1;
        double val2;
        double val3, val4, val5, val6;
        String k;
        String rate = "0";
        String p = "1";
        String d = "2";
        System.out.println("Для просмотра курса валют введите 0!\nДля перевода рублей в иностранную валюту введите 1!\nДля перевода долларов США введите 2!");

        k = input.nextLine();

        if (k.equals(rate)) {
            System.out.println("1 USD (Доллар США) = " + usd + " RUB\n");
            System.out.println("1 EUR (Евро) = " + eur + " RUB\n");
            System.out.println("1 GBP (Фунт стерлингов Соединенного королевства) = " + gbp + " RUB\n");
            System.out.println("1 CAD (Канадский доллар) = " + cad + " RUB\n");
            System.out.println("1 BRL (Бразильский реал) = " + brl + " RUB\n");
            System.out.println("1 CHF (Швейцарский франк) = " + chf + " RUB\n");
        } else if (k.equals(p)) {

            System.out.println("Перевод рублей в иностранную валюту!\n" + "Введите сумму:");
            summa = input.nextDouble();

            val1 = summa / usd;
            val2 = summa / eur;
            val3 = summa / gbp;
            val4 = summa / cad;
            val5 = summa / brl;
            val6 = summa / chf;

            System.out.printf("%1$.2f RUB = %2$.2f USD\n", summa, val1);
            System.out.printf("%1$.2f RUB = %2$.2f EUR\n", summa, val2);
            System.out.printf("%1$.2f RUB = %2$.2f GBP\n", summa, val3);
            System.out.printf("%1$.2f RUB = %2$.2f CAD\n", summa, val4);
            System.out.printf("%1$.2f RUB = %2$.2f BRL\n", summa, val5);
            System.out.printf("%1$.2f RUB = %2$.2f CHF\n", summa, val6);

        } else if (k.equals(d)) {

            System.out.println("Перевод долларов США (USD)!\n" + "Введите сумму:");
            summa = input.nextDouble();
            val1 = summa * usd;
            val2 = summa * usd / eur;
            val3 = summa * usd / gbp;
            val4 = summa * usd / cad;
            val5 = summa * usd / brl;
            val6 = summa * usd / chf;

            System.out.printf("%1$.2f USD = %2$.2f RUB\n", summa, val1);// перевод в рубли
            System.out.printf("%1$.2f USD = %2$.2f EUR\n", summa, val2); // в евро
            System.out.printf("%1$.2f USD = %2$.2f GBP\n", summa, val3); // в фунты стерлингов
            System.out.printf("%1$.2f USD = %2$.2f CAD\n", summa, val4); // в канадский доллар
            System.out.printf("%1$.2f USD = %2$.2f BRL\n", summa, val5); // в Бразильский реал
            System.out.printf("%1$.2f USD = %2$.2f CHF\n", summa, val6); // в Швейцарский франк


        }

    }
}

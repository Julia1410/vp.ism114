package ru.mirea.iis.vp.zzz.labs.first;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

/**
 *
 * @author zzz
 */
public class TcpServer {

    private static final Logger logger = Logger.getLogger(TcpServer.class.getName());
    private final IoAcceptor acceptor = new NioSocketAcceptor(Runtime.getRuntime().availableProcessors());
    private final CopyOnWriteArrayList<String> messages = new CopyOnWriteArrayList<>();

    public void startServer(int port) throws IOException {
        acceptor.getFilterChain().addLast("codec",
                                          new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"))));
        acceptor.setHandler(new BlinkListerHandler());
        acceptor.getSessionConfig().setReadBufferSize(2048);
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
        acceptor.bind(new InetSocketAddress(port));
        logger.log(Level.INFO, "Started TCP server on port {0}", port);
    }

    public void stopServer() {
        if (acceptor.isActive()) {
            acceptor.unbind();
        }
        logger.log(Level.INFO, "TCP server has stoped");
    }

    public String getLastMessage() {
        return messages.get(messages.size());
    }

    public CopyOnWriteArrayList<String> getMessages() {
        return messages;
    }

    private class BlinkListerHandler extends IoHandlerAdapter {

        @Override
        public void messageReceived(IoSession session, Object message) throws Exception {
            messages.add(message.toString());
            logger.log(Level.INFO, "Recieved message {0}", message.toString());
            if (message.toString().equals("stop")) {
                stopServer();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        TcpServer server = new TcpServer();
        server.startServer(9999);
    }
}
